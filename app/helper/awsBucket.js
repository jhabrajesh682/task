const AWS = require('aws-sdk');
const intoStream = require('into-stream');


const BUCKET_NAME = process.env.BUCKET_NAME;
const USER_KEY = process.env.USER_KEY;
const USER_SECRET = process.env.USER_SECRET;
const EXPIRE = parseInt(process.env.EXPIRE) * 60 * 24 * 15;

const s3bucket = new AWS.S3({
    accessKeyId: USER_KEY,
    secretAccessKey: USER_SECRET,
});

const getBlobName = (originalName) => {
    const identifier = Math.random().toString().replace(/0\./, ''); // remove "0." from start of string
    return `${identifier}-${originalName}`;
};


function upload(buffer, fileName) {
    console.log("into upload");
    const stream = intoStream(buffer);
    const blobName = getBlobName(fileName);
    const params = {
        Bucket: BUCKET_NAME,
        Key: blobName,
        Body: stream,
    };

    
    return new Promise((resolve, reject) => {
        s3bucket.upload(params, function (err, data) {
            if (err) reject(err);
            console.log("file uploaded🔥🔥🔥 ");
            console.log(data);
            resolve({ message: 'File uploaded! :' + blobName, data: data });
        });
    });
}

function getfile(key) {
    return s3bucket.getSignedUrl('getObject', {
        Bucket: BUCKET_NAME,
        Key: key,
        Expires: EXPIRE,
    });
}

module.exports = { upload, getfile };
const fileSchema = require("../models/file.model");
const validate = require("../validators/file.validator");
let {
    upload,
    getfile,
} = require('../helper/awsBucket');

class file { 

    async createFileUpload(req,res) { 
        console.log("req.body",req.body);
        let { error } = validate.validateAddFile(req.body);
        if (error) {
            res.status(400).send({
                message: "failed",
                result: error
            })
        }


        const sampleFile = req.files.orignalName;
        const buffer = sampleFile.data;
        const filename = sampleFile.name;
        let data = await upload(buffer, filename);

        console.log("sampleFile", sampleFile);
        console.log("buffer", buffer);
        console.log("filename", filename);

        let file = new fileSchema({
            Name: req.body.Name,
            photo: {
                orignalName:filename ,
                blobName:data.data.key,
            }
        })
        console.log("fileuploaded 🔥 ");
        
        await file.save();
        res.status(200).send({
            message: "fileupload is created",
            status: true,
            result:file
        })
    }

    async getAllFile(req, res) { 
        
        let limit
        let page
        if (req.query.limit) {
            limit = (parseInt(req.query.limit) ? parseInt(req.query.limit) : 10);
            page = req.query.page ? (parseInt(req.query.page) ? parseInt(req.query.page) : 1) : 1;
        }
        const createdAt = req.query.createdAt ? (req.query.createdAt == 'desc' ? -1 : 1) : 1

        let file = await fileSchema.find({})
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({ createdAt: createdAt })
            .lean()
        
        res.status(200).send({
            status: true,
            file:file
        })
    }

    async viewFile(req, res) { 
        let blob = req.params.blobName

        let sasUrl = getfile(blob);
        res.status(200).send({
            status: true,
            url:sasUrl
        })
    }

    async GetOneAndDeleteFile(req,res) {
        let fileId = req.params.id;
        let file = await fileSchema.findByIdAndRemove(fileId).lean();

        let status = false
        if (status) {
            status = true
        }
        res.status(200).send({
            message: "file successfully deleted",
            result: file
        })
    }
}

module.exports = file
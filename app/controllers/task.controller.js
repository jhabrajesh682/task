const taskSchema = require("../models/task.model");
const validate = require("../validators/task.validator");


class task { 


    /**
     * create Task
     * update Task
     * Delete Task
     * Get All Task (first uncompleted Task and Then completed Task) 
     */
    async createTask(req, res) {
        let { error } = validate.validateAddTask(req.body);
        if (error) {
            res.status(400).send({
                message: "failed",
                result: error
            })
        }

        let task = new taskSchema({
            ...req.body
        })

        await task.save();
        res.status(200).send({
            message: "Task successfully created",
            status: true,
            result: task
        })
    }



    //update task API
    
    async getOneTaskAndUpdate(req, res) {
        console.log("bpody====>", req.body);
        let taskId = req.params.id;
        let task = await taskSchema.findById(taskId).lean();
        if (!task) {
            res.status(404).send({
                status: false,
                message: "task doesn't exist",
                
            })
        }

        let { error } = validate.validateUpdateTasks(req.body)
        if (error) {
            res.status(400).send({
                message: "failed",
                result: error
            })
        }

        let taskUpdated = await taskSchema.findByIdAndUpdate(taskId, req.body, {
            new:true
        })

        res.status(200).send({
            message: "task is updated",
            status: true,
            result: taskUpdated
        })
    }

    //Delete Task
    async getOneTaskAndDelete(req, res) {
        let taskId = req.params.id;
        let task = await taskSchema.findByIdAndRemove(taskId).lean();

        let status = false
        if (status) {
            status = true
        }
        res.status(200).send({
            message: "task successfully deleted",
            result: task
        })

    }


    async getAllTask(req,res) { 
        let limit
        let page
        if (req.query.limit) {
            limit = (parseInt(req.query.limit) ? parseInt(req.query.limit) : 10);
            page = req.query.page ? (parseInt(req.query.page) ? parseInt(req.query.page) : 1) : 1;
        }

        let task = await taskSchema.find({ status:"uncompleted"})
           
        let task1 = await taskSchema.find({ status: "Completed" })
        let finalTask = task.concat(task1)
        res.status(200).send({
            status: true,
            task: finalTask
        })
    }
}

module.exports = task

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fileSchema = new Schema({

    Name: {
        type: String,
        required: true
    },

    photo: {
        type: Object,
        required:true
    },
},
    { 
        timestamps:true
    }
)

module.exports = file = mongoose.model("file", fileSchema);
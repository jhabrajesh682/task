const joi = require("@hapi/joi");

function validateUsers(user) {
    let schema = joi.object({
        Name: joi.string().required(),
        roleName: joi.string().required(),
        Email: joi.string().max(50).email().required(),
        username: joi.string().min(5).required(),
        password: joi.string().required()
    })
    let result = schema.validate(user)
    console.log("result ❌ ", result);
    return result
}

function validateUpdateUser(user) {
    let schema = joi.object({
        Name: joi.string(),
        roleName: joi.string(),
        Email: joi.string().max(50).email(),
        username: joi.string().min(5),
        password: joi.string()
    })
    let result = schema.validate(user)
    return result
}
function authenticateUser(user) {
    let schema = joi.object({
        Email: joi.string().required(),
        password: joi.string().required()
    })
    let result = schema.validate(user)
    return result
}


module.exports.validateUsers = validateUsers
module.exports.validateUpdateUser = validateUpdateUser
module.exports.authenticateUser = authenticateUser
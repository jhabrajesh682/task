const router = require("express").Router();
const task = require("../controllers/task.controller");
const auth = require("../middlewares/auth");

const tasks = new task();

/****************************************API Routes for task************************************/

router.get("/", [auth],tasks.getAllTask);

router.post("/", [auth], tasks.createTask);


router.put("/:id", [auth],tasks.getOneTaskAndUpdate);

router.delete("/:id", [auth], tasks.getOneTaskAndDelete);

module.exports = router;
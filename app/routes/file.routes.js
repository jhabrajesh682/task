const router = require("express").Router();
const file = require("../controllers/file.controller");
const auth = require("../middlewares/auth");


const files = new file();


router.get("/", files.getAllFile);

router.post("/", files.createFileUpload);

router.get("/SASUrl/:blobName", files.viewFile);

router.delete("/:id", files.GetOneAndDeleteFile);

module.exports = router;